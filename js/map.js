
var DOPAgetWdpaExtent = "https://rest-services.jrc.ec.europa.eu/services/d6biopamarest/d6biopama/get_wdpa_extent?format=json&wdpa_id=";
var DOPAgetCountryExtent = "https://rest-services.jrc.ec.europa.eu/services/d6biopamarest/administrative_units/get_country_extent_by_iso?format=json&a_iso=";

var regionSummary = {};
var countrySummary = {};
var firstMapLoad = 0;
var globalKlcData;
var activeKLC = "";

var selSettings = {
    paName: 'default',
	WDPAID: 0,
    countryName: 'trans-ACP',
    regionID: null,
	regionName: null,
	ISO2: null,
	ISO3: null,
	NUM: null,
};

jQuery(document).ready(function($) {

	var mymap = $().createMap("map-container", {
		center: [26, -6.66],
		zoom: 2,
	});


	function resizeContainer(height){
		$('#klc-accordion').css('height', height-80);
	}
	
	var height = $().getWindowHeight();
	resizeContainer(height);
	

	var klcMapOptions = {padding: {top: 10, bottom:10, left: 10, right: 10}}
	mymap.fitBounds([
		[-4.136719,-45.959409],
		[35.589844,45.959409]
	], klcMapOptions);

	
	
	$().addMapControls(mymap, "satelliteToggle");
	$().addMapControls(mymap, "loaderControl");
	$().addMapControls(mymap, "fullScreen");
	$().addMapControls(mymap, "navigation");	

	$().addMapLayerBiopamaSources(mymap);

	$().addMapLayer(mymap, "biopamaGaulEez");
	$().addMapLayer(mymap, "biopamaCountries");
	$().addMapLayer(mymap, "biopamaWDPAPolyJRC");
	$().addMapLayer(mymap, "biopamaWDPAPoint");
	$().addMapLayer(mymap, "satellite");
	



	mymap.on('load', function () {
		let attribution = "KLC info! <a href='http://www.google.com'>Temp link to Google</a>";
		let tiles = ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?layer=biopama:klc_201909_proposal&tilematrixset=EPSG:900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=application/x-protobuf;type=mapbox-vector&TileMatrix=EPSG:900913:{z}&TileCol={x}&TileRow={y}"];
		mymap.addSource("klc-source",{
			"attribution": attribution,
			"type": "vector",
			"tiles": tiles 
		  }
		); 
		mymap.addLayer({
			'id': 'klc-layer',
			'type': 'fill',
			'source': "klc-source",
			'source-layer': 'klc_201909_proposal',
			'paint': {
				"fill-color": "hsla(85, 86%, 38%, 0.5)",
			},
		});
		mymap.addLayer({
			'id': 'klc-layer-hover',
			'type': 'line',
			'source': "klc-source",
			'source-layer': 'klc_201909_proposal',
			"layout": {"visibility": "none"},
			"paint": {
				
				"line-color": "#84959a",
				"line-width": 2,
			}
		});
		mymap.addLayer({
			'id': 'klc-layer-selected',
			'type': 'line',
			'source': "klc-source",
			'source-layer': 'klc_201909_proposal',
			"layout": {"visibility": "none"},
			"paint": {
				"line-color": "#84959a",
				"line-width": 3,
			}
		}, 'klc-layer-hover');

	});
	mymap.on('idle', () => {
		if (firstMapLoad == 0){
			firstMapLoad = 1;
			getallKlcData();
			filterKLCs();
		}
		
	});
	
	var popup = new mapboxgl.Popup({
		closeButton: false,
		closeOnClick: false
	});
	
	function getallKlcData(){
		var features = mymap.queryRenderedFeatures({layers: ['klc-layer']});	
		var displayFeatures = features.map(function(feat) {
			var displayFeat = {};
			displayFeat = feat["properties"];
			return displayFeat;
		});
		var unique = _.uniqWith(displayFeatures, _.isEqual);
		globalKlcData = unique.sort(function(a, b) {
			var nameA = a.klc_name.toUpperCase();
			var nameB = b.klc_name.toUpperCase();
			if (nameA < nameB) {
				return -1;
			}
			if (nameA > nameB) {
				return 1;
			}
			return 0;
		});
	};
	
	function getKlcData(KLC){
		return globalKlcData.find(x => x.klc_name === KLC)
	}

	function getKlcDataID(KLC){
		return globalKlcData.find(x => x.klc_id === KLC)
	}
	
	
	function updateKLClist(KLCs){
		$('.wrapper-card').on('show.bs.collapse', function () {
				//alert($("button.biopama-collapse[aria-expanded='true']").text().trim())
				activeKLC = $(this).attr('id');
				activeKLC = activeKLC.replace('collapse', '');
				//alert(activeKLC);
				var klcFeatures = getKlcDataID(activeKLC);
				//console.log(activeKLC)
				$('#klc-accordion').scrollTo('#'+klcFeatures.klc_id, {duration:500});
				
				mymap.setFilter('klc-layer-selected', ['==', 'klc_name', klcFeatures.klc_name]);
				mymap.setLayoutProperty('klc-layer-selected', 'visibility', 'visible');
				mymap.flyTo({
					center: [klcFeatures.centroid_x, klcFeatures.centroid_y],
					zoom: 5,
					essential: true // this animation is considered essential with respect to prefers-reduced-motion
				});

				var klcTargetPAs = klcFeatures.target_pas;
				klcTargetPAs = klcTargetPAs.slice(1,-1).split(', ');
				var klcPANames = klcFeatures.pa;
				klcPANames = klcPANames.slice(1,-1).replace('\\', '').replace(/['"]+/g, '').split(',');
				var klcPAIDs = klcFeatures.wdpaid;
				klcPAIDs = klcPAIDs.slice(1,-1).split(',');
				klcPAIDs.forEach((currentValue, index) => { 	
					klcTargetPAs.forEach((targetValue, targetIndex) => { 
						if (currentValue == klcTargetPAs[targetIndex]){
							klcTargetPAs[targetIndex] = ' <a href="/ct/pa/' + currentValue + '" target="_blank">' + klcPANames[index] + '</a>';
						}
					});
					klcPAIDs[index] = ' <a href="/ct/pa/' + currentValue + '" target="_blank">' + klcPANames[index] + '</a>';
				} );
				var klcCountries =  klcFeatures.country;
				klcCountries = klcCountries.slice(1,-1).replace(', ', '- ').replace('\\', '').replace(/['"]+/g, '').split(',');
				var klcCountryCodes =  klcFeatures.iso2;
				klcCountryCodes = klcCountryCodes.slice(1,-1).split(',');
				klcCountryCodes.forEach((currentValue, index) => { 
					klcCountryCodes[index] = ' <a href="/ct/country/' + currentValue + '" target="_blank">' + klcCountries[index] + '</a>';
				} );
				var klcRegions =  klcFeatures.region;
				klcRegions = klcRegions.slice(1,-1).split(',');
				klcRegions.forEach((currentValue, index) => { 
					switch(currentValue){
						case 'eastern_africa':
							//klcRegions[index] = ' <a href="/region/' + currentValue + '" target="_blank">Eastern Africa</a>';
							klcRegions[index] = ' Eastern Africa';
							break;
						case 'western_africa':
							//klcRegions[index] = ' <a href="/region/' + currentValue + '" target="_blank">Western Africa</a>';
							klcRegions[index] = ' Western Africa';
							break;
						case 'southern_africa':
							//klcRegions[index] = ' <a href="/region/' + currentValue + '" target="_blank">Southern Africa</a>';
							klcRegions[index] = ' Southern Africa';
							break;
						case 'central_africa':
							//klcRegions[index] = ' <a href="/region/' + currentValue + '" target="_blank">Central Africa</a>';
							klcRegions[index] = ' Central Africa';
							break;
						default:
							//klcRegions[index] = ' <a href="/region/' + currentValue + '" target="_blank">' + currentValue + '</a>';
							klcRegions[index] = ' ' + currentValue;
					}
				} );
				var klcSignificance = klcFeatures.signif;
				klcSignificance = klcSignificance.slice(1,-1).replace(/['"]+/g, '').split(',');
				klcSignificance.forEach((currentValue, index) => { 
					klcSignificance[index] = '<li>' + currentValue + '</li>';
				} );
				klcSignificance = klcSignificance.join('');
				var klcEcoIDs = klcFeatures.ecoregion_id;
				klcEcoIDs = klcEcoIDs.slice(1,-1).split(',');
				var klcEcoregions = klcFeatures.ecoregion;
				klcEcoregions = klcEcoregions.slice(1,-1).replace('\\', '').replace(/['"]+/g, '').split(',');
				klcEcoregions.forEach((currentValue, index) => { 
					klcEcoregions[index] = ' <a href="https://dopa-explorer.jrc.ec.europa.eu/ecoregion/' + klcEcoIDs[index] + '" target="_blank">' + klcEcoregions[index] + '</a>';
				} );
				var klcCopernLink = klcFeatures.is_copernicushs;
				if (klcCopernLink == 'true'){
					klcCopernLink = "<tr><td>Copernicus Hot Spot</td><td> <a href='https://land.copernicus.eu/global/hsm' target='_blank'>Link</a></td></tr>";
				} else { 
					klcCopernLink = '';
				}
				var klcTfcaLink = klcFeatures.is_tfca;
				if (klcTfcaLink == 'true'){
					klcTfcaLink = "<tr><td>TFCA</td><td> <a href='https://tfcaportal.org/' target='_blank'>Link</a></td></tr>";
				} else { 
					klcTfcaLink = '';
				}

				KLCinfo = "<div>"+
					"<div class='row'>"+
						"<div class='col-lg-12'>"+
							"<table>"+
								"<tr><td>Area</td><td id='areaVal'>"+parseFloat(klcFeatures.area_km2).toFixed(3)+"km&sup2;</td></tr>"+
								
								"<tr><td>Regions </td><td id='regVal'>"+ klcRegions + "</td></tr>"+
								"<tr><td>Countries</td><td id='countryVal'>"+ klcCountryCodes +"</td></tr>"+
								"<tr><td>Target Protected Areas</td><td id='paVal'>"+klcTargetPAs+"</td></tr>"+
								"<tr><td>Ecoregions</td><td id='ecoVal'>"+klcEcoregions+"</td></tr>"+
								klcCopernLink+
								klcTfcaLink+
								"<tr><td>Significance</td><td id='sigVal'><ul class='signif'>"+klcSignificance+"</ul></td></tr>"+
								"<tr><td>KLC ID</td><td id='idVal'>"+klcFeatures.klc_id+"</td></tr>"+

							"</table>"+
						"</div>"+
					"</div>"+
					"<div class='row'>"+
						"<div class='col-lg-12'>"+
							"<h3 class='klc-chart-title'>"+
								"Land cover fractions from 2015-2019 in " + klcFeatures.klc_name +
							"</h3>"+
							"<div class='klc-chart' id='api_klc_cglc"+klcFeatures.klc_id+"'>"+
							"</div>"+
							"<div class='klc-source'>"+
								"Data source: <a href='https://lcviewer.vito.be/'>Copernicus global land service</a>" +
							"</div>"+
						"</div>"+
					"</div>"+
					"<div class='row'>"+
						"<div class='col-lg-6'>"+
							"<div class='klc-chart' id='api_klc_countries"+klcFeatures.klc_id+"'>"+
							"</div>"+
							"<div class='klc-source'>"+
								"Data source: <a href='http://www.fao.org/geonetwork/srv/en/metadata.show?id=12691'>Global Administrative Unit Layers (GAUL), revision 2015 and EEZ</a> and <a href='http://www.marineregions.org/downloads.php'>Exclusive Economic Zones (EEZ) v9 (2016-10-21)</a>" +
							"</div>"+
						"</div>"+
						"<div class='col-lg-6'>"+
							"<div class='klc-chart' id='api_klc_ecoregions"+klcFeatures.klc_id+"'>"+
							"</div>"+
							"<div class='klc-source'>"+
								"Data source: <a href='https://www.worldwildlife.org/publications/terrestrial-ecoregions-of-the-world'>Terrestrial Ecoregions of the World (Olson et al. 2001)</a>" +
							"</div>"+
						"</div>"+
					"</div>"+
					"<div class='row'>"+
						"<div class='col-12'>"+
							"<h3 class='klc-chart-title'>"+
								"Protected Areas in " + klcFeatures.klc_name +
							"</h3>"+
							"<div class='wrapper-pa-data-table'><table class='pa-data-table display' id='pa-data-table-"+klcFeatures.klc_id+"' width='100%'></table></div>"+
							"<div class='klc-source klc-source_pa'>"+
								"Data source: <a href='https://protectedplanet.net'>WorldDatabase on Protected Areas January 2021 (UNEP-WCMC & IUCN, 2021)</a>" +
							"</div>"+ 
						"</div>"+
					"</div>"+
				"</div>";
				//ui.newPanel[0].innerHTML = KLCinfo;
				$("div#collapse" + klcFeatures.klc_id + " .card-text").html(KLCinfo);

				//mymap.setLayoutProperty('klc-layer-selected', 'visibility', 'none');	
			
		});


		$('.wrapper-card').on('shown.bs.collapse', function () {
			var klcFeatures = getKlcDataID(activeKLC);
			//console.log(klcFeatures)
			
			var api_klc_cglc = 'https://api.biopama.org/api/klc/function/api_klc_cglc/klc_id='+klcFeatures.klc_id;
			$.getJSON(api_klc_cglc,function(d){
				var dataSetTotal = [];
				var dataSetPaIn = [];
				var dataSetPaOut = [];				
				$(d).each(function(i, data) {
					if (data.pa == 'inside'){
						dataSetPaIn.push(data); 						
					} else if (data.pa == 'outside') {
						dataSetPaOut.push(data); 
					} else {
						dataSetTotal.push(data); 
					}
				});
				var indicatorChart = echarts.init(document.getElementById('api_klc_cglc'+klcFeatures.klc_id));
				var option = {
					dataset: [{
						dimensions: ["klc_id", "pc_moss", "pc_bare", "pc_tree", "pc_water_seasonal", "urban", "pc_snow", "pc_shrub", "area", "moss", "pa", "pc_urban", "year", "water_seasonal", "water_permanent", "pc_water_permanent", "bare", "tree", "crops", "pc_grass", "shrub", "grass", "pc_crops", "snow"],
						source: dataSetPaIn
					}, {
						dimensions: ["klc_id", "pc_moss", "pc_bare", "pc_tree", "pc_water_seasonal", "urban", "pc_snow", "pc_shrub", "area", "moss", "pa", "pc_urban", "year", "water_seasonal", "water_permanent", "pc_water_permanent", "bare", "tree", "crops", "pc_grass", "shrub", "grass", "pc_crops", "snow"],
						source: dataSetPaOut
					}, {
						dimensions: ["klc_id", "pc_moss", "pc_bare", "pc_tree", "pc_water_seasonal", "urban", "pc_snow", "pc_shrub", "area", "moss", "pa", "pc_urban", "year", "water_seasonal", "water_permanent", "pc_water_permanent", "bare", "tree", "crops", "pc_grass", "shrub", "grass", "pc_crops", "snow"],
						source: dataSetTotal
					}],
					tooltip: {
						trigger: 'axis',
						axisPointer: {
							type: 'shadow'
						}
					},
					legend: {
						textStyle: {color: "#aaa" },
					},
					grid: [
						{left: '6%', right:'70%'},
						{left: '38%', right:'38%'},
						{left: '70%', right:'6%'}
					],
					//color: ['#ab2828', '#ed7325', '#ffd954', '#b8d879', '#46a246','#c2c5cc'],
					xAxis: [{
						type: 'category',
						name: 'Year',
						nameLocation: "middle",
						nameGap: 30,
						gridIndex: 0
					},{
						type: 'category',
						name: 'Year',
						nameLocation: "middle", 
						nameGap: 30,
						gridIndex: 1
					},{
						type: 'category',
						name: 'Year',
						nameLocation: "middle", 
						nameGap: 30,
						gridIndex: 2
					}],
					yAxis: [{
						type: 'value',
						name: 'Inside PAs', 
						nameGap: 10,
						max: 'dataMax',
						gridIndex: 0
					},{
						type: 'value',
						name: 'Outside PAs', 
						nameGap: 10,
						max: 'dataMax',
						gridIndex: 1
					},{
						type: 'value',
						name: 'Total', 
						nameGap: 10,
						max: 'dataMax',
						gridIndex: 2
					}],
					//				0			1			2		3				4				5		6			7			8		9		10		11			12			13				14				15					16		17		18			19			20		21		22			23	
					//dimensions: ["klc_id", "pc_moss", "pc_bare", "pc_tree", "pc_water_seasonal", "urban", "pc_snow", "pc_shrub", "area", "moss", "pa", "pc_urban", "year", "water_seasonal", "water_permanent", "pc_water_permanent", "bare", "tree", "crops", "pc_grass", "shrub", "grass", "pc_crops", "snow"],
					series: [{
						name: 'Moss', encode: { x: 12, y: 1 },
						type: 'bar', stack: 'group', datasetIndex: 0, xAxisIndex: 0, yAxisIndex: 0
					},{
						name: 'Bare', encode: { x: 12, y: 2 },
						type: 'bar', stack: 'group', datasetIndex: 0, xAxisIndex: 0, yAxisIndex: 0
					},{
						name: 'Tree', encode: { x: 12, y: 3 },
						type: 'bar', stack: 'group', datasetIndex: 0, xAxisIndex: 0, yAxisIndex: 0
					},{
						name: 'Snow', encode: { x: 12, y: 6 },
						type: 'bar', stack: 'group', datasetIndex: 0, xAxisIndex: 0, yAxisIndex: 0
					},{
						name: 'Shrub', encode: { x: 12, y: 7 },
						type: 'bar', stack: 'group', datasetIndex: 0, xAxisIndex: 0, yAxisIndex: 0
					},{
						name: 'Urban', encode: { x: 12, y: 11 },
						type: 'bar', stack: 'group', datasetIndex: 0, xAxisIndex: 0, yAxisIndex: 0
					},{
						name: 'Grass', encode: { x: 12, y: 19 },
						type: 'bar', stack: 'group', datasetIndex: 0, xAxisIndex: 0, yAxisIndex: 0
					},{
						name: 'Crops', encode: { x: 12, y: 22 },
						type: 'bar', stack: 'group', datasetIndex: 0, xAxisIndex: 0, yAxisIndex: 0
					},{
						name: 'Water Seasonal', encode: { x: 12, y: 4 },
						type: 'bar', stack: 'group', datasetIndex: 0, xAxisIndex: 0, yAxisIndex: 0
					},{
						name: 'Water Permanent', encode: { x: 12, y: 15 },
						type: 'bar', stack: 'group', datasetIndex: 0, xAxisIndex: 0, yAxisIndex: 0
					},{
						name: 'Moss', encode: { x: 12, y: 1 },
						type: 'bar', stack: 'group2', datasetIndex: 1, xAxisIndex: 1, yAxisIndex: 1
					},{
						name: 'Bare', encode: { x: 12, y: 2 },
						type: 'bar', stack: 'group2', datasetIndex: 1, xAxisIndex: 1, yAxisIndex: 1
					},{
						name: 'Tree', encode: { x: 12, y: 3 },
						type: 'bar', stack: 'group2', datasetIndex: 1, xAxisIndex: 1, yAxisIndex: 1
					},{
						name: 'Snow', encode: { x: 12, y: 6 },
						type: 'bar', stack: 'group2', datasetIndex: 1, xAxisIndex: 1, yAxisIndex: 1
					},{
						name: 'Shrub', encode: { x: 12, y: 7 },
						type: 'bar', stack: 'group2', datasetIndex: 1, xAxisIndex: 1, yAxisIndex: 1
					},{
						name: 'Urban', encode: { x: 12, y: 11 },
						type: 'bar', stack: 'group2', datasetIndex: 1, xAxisIndex: 1, yAxisIndex: 1
					},{
						name: 'Grass', encode: { x: 12, y: 19 },
						type: 'bar', stack: 'group2', datasetIndex: 1, xAxisIndex: 1, yAxisIndex: 1
					},{
						name: 'Crops', encode: { x: 12, y: 22 },
						type: 'bar', stack: 'group2', datasetIndex: 1, xAxisIndex: 1, yAxisIndex: 1
					},{
						name: 'Water Seasonal', encode: { x: 12, y: 4 },
						type: 'bar', stack: 'group2', datasetIndex: 1, xAxisIndex: 1, yAxisIndex: 1
					},{
						name: 'Water Permanent', encode: { x: 12, y: 15 },
						type: 'bar', stack: 'group2', datasetIndex: 1, xAxisIndex: 1, yAxisIndex: 1
					},{
						name: 'Moss', encode: { x: 12, y: 1 },
						type: 'bar', stack: 'group3', datasetIndex: 2, xAxisIndex: 2, yAxisIndex: 2
					},{
						name: 'Bare', encode: { x: 12, y: 2 },
						type: 'bar', stack: 'group3', datasetIndex: 2, xAxisIndex: 2, yAxisIndex: 2
					},{
						name: 'Tree', encode: { x: 12, y: 3 },
						type: 'bar', stack: 'group3', datasetIndex: 2, xAxisIndex: 2, yAxisIndex: 2
					},{
						name: 'Snow', encode: { x: 12, y: 6 },
						type: 'bar', stack: 'group3', datasetIndex: 2, xAxisIndex: 2, yAxisIndex: 2
					},{
						name: 'Shrub', encode: { x: 12, y: 7 },
						type: 'bar', stack: 'group3', datasetIndex: 2, xAxisIndex: 2, yAxisIndex: 2
					},{
						name: 'Urban', encode: { x: 12, y: 11 },
						type: 'bar', stack: 'group3', datasetIndex: 2, xAxisIndex: 2, yAxisIndex: 2
					},{
						name: 'Grass', encode: { x: 12, y: 19 },
						type: 'bar', stack: 'group3', datasetIndex: 2, xAxisIndex: 2, yAxisIndex: 2
					},{
						name: 'Crops', encode: { x: 12, y: 22 },
						type: 'bar', stack: 'group3', datasetIndex: 2, xAxisIndex: 2, yAxisIndex: 2
					},{
						name: 'Water Seasonal', encode: { x: 12, y: 4 },
						type: 'bar', stack: 'group3', datasetIndex: 2, xAxisIndex: 2, yAxisIndex: 2
					},{
						name: 'Water Permanent', encode: { x: 12, y: 15 },
						type: 'bar', stack: 'group3', datasetIndex: 2, xAxisIndex: 2, yAxisIndex: 2
					}],
				};
				indicatorChart.setOption(option);
			});
			
			//KLC Countries
			var api_klc_countries = 'https://api.biopama.org/api/klc/function/api_klc_countries/klc_id='+klcFeatures.klc_id;
			$.getJSON(api_klc_countries,function(d){
				var indicatorTitle = "Percentage of " + klcFeatures.klc_name + " in countries";
				var chartData = {
					title: indicatorTitle, 
					dataset: {
						dimensions: ['pc_country_in_klc', 'pc_klc_in_country', 'klc_id', 'country_name', 'iso3', 'area_klc_country'],
						source: d
					},
					series: [{
						type: 'pie',
						radius: [20, 60],
						encode: {
							itemName: 'country_name',
							value: 'pc_klc_in_country'
						}
					}]
				};
				$().createNoAxisChart('api_klc_countries'+klcFeatures.klc_id , chartData); 
			});

			//KLC Ecoregions
			var api_klc_ecoregions = 'https://api.biopama.org/api/klc/function/api_klc_ecoregions/klc_id='+klcFeatures.klc_id;
			$.getJSON(api_klc_ecoregions,function(d){
				var indicatorTitle = "Percentage of " + klcFeatures.klc_name + " in ecoregions";
				var chartData = {
					title: indicatorTitle, 
					dataset: {
						dimensions: ['klc_id', 'area_klc_ecoregion', 'pc_ecoregion_in_klc', 'pc_klc_in_ecoregion', 'first_level_code', 'first_level'],
						source: d
					},
					series: [{
						type: 'pie',
						radius: [20, 60],
						encode: {
							itemName: 'first_level',
							value: 'pc_klc_in_ecoregion'
						}
					}]
				};
				$().createNoAxisChart('api_klc_ecoregions'+klcFeatures.klc_id , chartData); 
			});
			//KLC PAs
			var api_klc_pas = 'https://api.biopama.org/api/klc/function/api_klc_pas/klc_id='+klcFeatures.klc_id;
			$.getJSON(api_klc_pas,function(d){
				var paDataSet = [];
				$(d).each(function(i, data) {
					var newDataRow = [];
					newDataRow.push( "<a href='/ct/pa/"+data.wdpaid+"' target='_blank'>" + data.pa_name + "</a>", data.desig_type, data.iucn_cat, data.desig_eng, data.pc_pa_in_klc, data.pc_klc_in_pa);
					paDataSet.push(newDataRow); 
				});
				var columnData = [
					{title: "Protected Area"},
					{title: "Type"},
					{title: "IUCN Category"},
					{title: "PA Designation"},
					{title: "% of PA in KLC"},
					{title: "% of KLC in PA"},
				];
				var dataTableID = "#pa-data-table-"+klcFeatures.klc_id;

				var tableData = {
					title: "Protected areas in the KLC",
					columns: columnData,
					data: paDataSet,
					isComplex: true,
					attribution: "<a ref='https://protectedplanet.net'>WorldDatabase on Protected Areas January 2021 (UNEP-WCMC & IUCN, 2021)</a>"
				}
				if ($.fn.DataTable.isDataTable( dataTableID ) ) { //to see if a datatable is here. Kill it if it is found
					var table = $(dataTableID).DataTable();
					table.destroy();
					$(dataTableID).empty();
				}
				
				$().createDataTable(dataTableID, tableData); 

			});
		  
			
		});

		$( "button.biopama-collapse" ).mouseenter(function() {
			var klcData = getKlcData($( this ).text().trim());		
			addKlcHover(klcData.klc_name);	
			popup.setLngLat([klcData.centroid_x,klcData.centroid_y]).setHTML(klcData.klc_name).addTo(mymap);
		}).mouseleave(function() {
			removeKlcHover();
		});
		
		mymap.on('mouseenter', 'klc-layer', function (e) {
			mymap.getCanvas().style.cursor = 'pointer';
			var coordinates = [e.features[0].properties.centroid_x, e.features[0].properties.centroid_y];
			var klc_name = e.features[0].properties.klc_name;
			addKlcHover(klc_name);	
			popup.setLngLat(coordinates).setHTML(klc_name).addTo(mymap);
		});
		mymap.on('mouseleave', 'klc-layer', function () {
			removeKlcHover();
		});
		mymap.on('click', 'klc-layer', function (e) {
			//find all accordion headers - as the search filter can change the number
			var klc_id = e.features[0].properties.klc_id;
			$(".wrapper-card.show").removeClass("show");
			$("button[data-bs-target='#collapse"+klc_id+"']").click();
			$('#klc-accordion').scrollTo('#collapse'+klc_id, {duration:500});
		});
		function addKlcHover(KLC){
			mymap.setFilter('klc-layer-hover', ['==', 'klc_name', KLC]);
			mymap.setLayoutProperty('klc-layer-hover', 'visibility', 'visible');
		}
		function removeKlcHover(){
			mymap.setLayoutProperty('klc-layer-hover', 'visibility', 'none');	
			mymap.getCanvas().style.cursor = '';
			popup.remove();
		}
		
		$( "#klc-description" ).accordion({
		  collapsible: false,
		  icons: { "header": "fas fa-chevron-circle-right", "activeHeader": "fas fa-chevron-circle-down" },
		  heightStyle: "content"
		});
		$( "#klc-accordion, #klc-description" ).show("fast");
	}

	$('select#klcRegions').change(function() {
		var selectedRegion = $( this ).val();
		zoomToRegion(selectedRegion);
		filterKLCs();
	});
	$('select#klcCountries').change(function() {
		var selectedCountry = $( this ).val();
		zoomToCountry(selectedCountry);
		filterKLCs();
	});
	$('#klcSearchTitle').on('keyup', function (e) {
		filterKLCs();
	});
	function filterKLCs(){
		var filteredKlcs = [];
		
		var searchKlcRegion = $('select#klcRegions').val();
		var searchKlcCountry = $('select#klcCountries').val();
		var searchKlcTitle = $('#klcSearchTitle').val().toLowerCase();

		if ($("#klc-accordion.ui-accordion").length > 0){
			$("#klc-accordion").accordion( "destroy" );
		}
		$("#klc-accordion").empty();

		var filteredKlcIds = ["in", "klc_id"];
		globalKlcData.forEach(function(klc) {
			var currentKlcRegion = klc.region;
			var currentKlcCountries = klc.iso3;
			var currentKlcTitle = klc.klc_name.toLowerCase();
			if(( ( searchKlcRegion == 'all' ) || ( currentKlcRegion.indexOf(searchKlcRegion) !== -1 ) ) && ( ( searchKlcCountry == 'all' ) || ( currentKlcCountries.indexOf(searchKlcCountry) !== -1 ) ) && ( currentKlcTitle.indexOf(searchKlcTitle) !== -1  )){
				//var newline = $( "<h3 id='" + klc.klc_id + "'>"+klc.klc_name+"</h3><div>Temp</div>" );
				var newline = $().createAccordion(klc.klc_id, klc.klc_name, "fa-solid fa-map-location", "klc-accordion");
				$( "#klc-accordion" ).append( newline );
				filteredKlcIds.push(klc.klc_id)
				filteredKlcs.push(klc);
			}
		});
		mymap.setFilter('klc-layer',filteredKlcIds);
		updateKLClist(filteredKlcs);
	}
	function zoomToRegion(region){
	  if(region === 'central_africa'){
		mymap.fitBounds([[1.8683898449,24.8886363352], [36.1896789074,-16.0012446593]], klcMapOptions);
	  } else if (region === 'eastern_africa'){
		mymap.fitBounds([[20.3034484386,26.6692628716], [54.6247375011,-14.0916051203]], klcMapOptions);
	  } else if (region === 'western_africa'){
		mymap.fitBounds([[-28.1462585926,31.1678846111], [20.4572570324,-0.7446243056]], klcMapOptions);
	  } else if (region === 'southern_africa'){
		mymap.fitBounds([[6.5265929699,-5.3073515284], [61.0187804699,-47.2924889494]], klcMapOptions);
	  } else if (region === 'pacific'){
		mymap.fitBounds([[123.75,-24.846565], [216.914063,18.312811]], klcMapOptions);
	  } else if (region === 'caribbean') {
		mymap.fitBounds([[-93.691406,-1.581830], [-51.240234,28.844674]], klcMapOptions);
	  } else {
		mymap.fitBounds([[-4.136719,-45.959409],[35.589844,45.959409]], klcMapOptions);
		  return;
	  }
	}
	function zoomToCountry(iso2){
	  if(iso2 === 'all'){
		mymap.fitBounds([[-4.136719,-45.959409],[35.589844,45.959409]], klcMapOptions);
	  } else if(iso2 === 'FJ'){
		mymap.fitBounds([[166.61,-26.39], [192.01,-9.62]], klcMapOptions);
	  } else if (iso2 === 'TV'){
		mymap.fitBounds([[168.58,-13.60], [191.48,-3.88]], klcMapOptions);
	  } else if (iso2 === 'KI'){
		mymap.fitBounds([[-201.57,-15.70], [-136.18,10.31]], klcMapOptions);
	  } else {
		jQuery.ajax({
		  url: DOPAgetCountryExtent+iso2,
		  dataType: 'json',
		  success: function(d) {
			mymap.fitBounds(jQuery.parseJSON(d.records[0].extent), klcMapOptions);
		  },
		  error: function() {
			console.log("Something is wrong with the REST servce for country bounds")
		  }
		});
	  }
	}
	
	
});
